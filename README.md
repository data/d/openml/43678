# OpenML dataset: Cryptocurrencies

https://www.openml.org/d/43678

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Thousands of cryptocurrencies have sprung up in the past few years. Can you predict which one will be the next BTC?
Content
The dataset contains daily opening, high, low, close, and trading volumes for over 1200 cryptocurrencies (excluding bitcoin).
Acknowledgements
https://timescaledata.blob.core.windows.net/datasets/crypto_data.tar.gz
Inspiration
Speculative forces are always at work on cryptocurrency exchanges - but do they contain any statistically significant features?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43678) of an [OpenML dataset](https://www.openml.org/d/43678). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43678/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43678/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43678/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

